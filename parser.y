%{
  #include <bits/stdc++.h>
  using namespace std;
  #include "visitor.h"

  extern "C" int yyparse();
  extern int lineNum;
  extern int yylex();
  extern FILE *yyin;
  
  void yyerror(const char *s);
  class ProgramStart* start = NULL;
  class Symboltable *symtable = NULL;
%}

%union{
	int intval;
	char* stringval;

    class ASTnode* ast;
    class ProgramStart* program;
    class DeclarationListNode* dec_list;
    class DeclarationNode* dec;
    class VarDecStatementNode* var_dec_stmnt;
	class TypeSpecifierNode* type;
	class FuncDecNode* func_dec;
	class FunctionArgsNode* func_args;
	class BlockNode* block;
	class StatementListNode* statement_list;
	class StatementNode* stmnt;
	class IfNode* ifstmnt;
	class WhileNode* whilestmnt;
	class ForNode* forstmnt;
	class JumpNode* jumpstmnt;
	class ReturnNode* returnstmnt;
	class BreakNode* breakstmnt;
	class ContNode* contstmnt;
	class AssignNode* assignstmnt;
	class LvalNode* lval;
	class FuncallNode* funcallstmnt;
	class ExprlistNode* expr_list;
	class PrintNode* printstmnt;
	class ReadNode* readstmnt;
	class ExprSuperNode* expr;
	class BinopNode* binopbin;
	class EnclosedNode* enclosedexpr;
	class OpNode* opnode;
	class BoolNode* boolnode;
	class IntNode* intnode;
	class CharNode* charnode;
}


%define parse.error verbose

%token IF ELSE
%token TRUE FALSE
%token FOR WHILE RETURN BREAK CONTINUE
%token INPUT PRINT
%token BOOL CHAR INT  VOID
%token SPECIAL


%type <ast> ast_node
%type <program> PROGRAM
%type <dec_list> DEC_LIST
%type <dec> DECL
%type <var_dec_stmnt> VARDECL
%type <type> TYPE
%type <func_dec> FUNCDECL
%type <func_args> ARGLIST
%type <block> BLOCK
%type <statement_list> STATLIST
%type <stmnt> STAT
%type <ifstmnt> IFSTM
%type <whilestmnt> WHILESTM
%type <forstmnt> FORSTM
%type <jumpstmnt> JUMPSTM
%type <assignstmnt> ASSIGNSTM
%type <lval> LVAL
%type <funcallstmnt> FUNCALLSTM
%type <expr_list> EXPRLIST 
%type <printstmnt> PRINTSTM
%type <readstmnt> READSTM
%type <expr> EXPR
%type <opnode> OP


%token <intnode> INT_VAR
%token <charnode> CHARLIST
%token <stringval>ARITHMETIC
%token <stringval>COMPARISON
%token <stringval>CONDITION
%token <stringval>BOOL_OP
%token <stringval>ASSIGNOP
%token <stringval>ID

%%

PROGRAM: DEC_LIST { $$ = $1; start = $$;};

DEC_LIST: DEC_LIST DECL {$$->insert($2);} 
		| DECL { $$ = new DeclarationListNode();
				$$->insert($1);
				}
		| %empty {$$ = new DeclarationListNode();}
		;

DECL: FUNCDECL {$$ = new DeclarationNode($1);} 
	| VARDECL ';' {$$ = new DeclarationNode($1);}
	;

VARDECL: TYPE ID { 
					$$ = new VarDecStatementNode($1, $2, NULL, NULL, 0);
				 }
	
	| TYPE ID '['']'  { 
						$$ = new VarDecStatementNode($1, $2, NULL, NULL, 3);
						}
	| TYPE ID '['']''['']'  { 
								$$ = new VarDecStatementNode($1, $2, NULL ,NULL , 4);
							}
	| TYPE ID '[' INT_VAR ']' {
							 $$ = new VarDecStatementNode($1, $2, $4, NULL, 1);

								} 
	| TYPE ID '[' INT_VAR ']' '[' INT_VAR ']' { 
											$$ = new VarDecStatementNode($1, $2, $4,$7, 2); 

												} 
	;


TYPE: INT  {$$ = new TypeSpecifierNode("INT");}
	| BOOL {$$ = new TypeSpecifierNode("BOOL");}
	| CHAR {$$ = new TypeSpecifierNode("CHAR");}
	| VOID {$$ = new TypeSpecifierNode("VOID");}
	;



FUNCDECL: TYPE ID '(' ARGLIST ')' BLOCK { 
											$$ = new FuncDecNode($1, $2, $4, $6);
										}
		;

ARGLIST: ARGLIST ',' VARDECL  {
								$$->insert($3);
							  }
	| VARDECL {
		        $$ = new FunctionArgsNode();
		        $$->insert($1);
			  }
	| %empty  {$$ = new FunctionArgsNode();}
	;

BLOCK: '{' DEC_LIST STATLIST '}' {
									$$ = new BlockNode($2,$3);
								 }		
	 ;

STATLIST: STATLIST STAT {
							$$->insert($2);
						}
		| STAT {
					$$ = new StatementListNode();
					$$->insert($1);
				}
		;

STAT: EXPR ';' {$$ = $1;}
	| IFSTM {$$ = $1;}
	| WHILESTM  {$$ = $1;}
	| FORSTM  {$$ = $1;}
	| JUMPSTM ';' {$$ = $1;}
	| ASSIGNSTM';' {$$ = $1;}
	| FUNCALLSTM ';' {$$ = $1;}
	| PRINTSTM';' {$$ = $1;}
	| READSTM';' {$$ = $1;}
	;




IFSTM: IF '(' EXPR ')' BLOCK {
								$$ = new IfNode($3,$5);
							}	 
	|  IF '(' EXPR ')' BLOCK ELSE BLOCK {
											$$ = new IfNode($3,$5,$7);
										}
	;


WHILESTM: WHILE '(' EXPR ')' BLOCK  {
										$$ = new WhileNode($3, $5);
									}
		;


FORSTM: FOR '(' ASSIGNSTM ';' EXPR ';' ASSIGNSTM ')' BLOCK { 
															$$ = new ForNode($3,$5,$7,$9);
														}
	;


JUMPSTM: RETURN {$$ = new ReturnNode();}
		| RETURN EXPR  {$$ = new ReturnNode($2);} 
		| BREAK {$$ = new BreakNode();}
		| CONTINUE {$$ = new ContNode();}
		;


ASSIGNSTM: LVAL '=' EXPR { 
							symtable->setter($1->id,$3->data); 
							$$ = new AssignNode($1,$3);
						}
		;

LVAL: ID {
			$$ = new LvalNode($1, NULL, NULL,"","",NULL);
		}
	
	| ID '[' ID ']' {
						$$ = new LvalNode($1, NULL, NULL, $3, "", NULL);
					}
	| ID '[' ID ']' '[' ID ']' {
									$$ = new LvalNode($1, NULL, NULL, $3, $6, NULL);
								}
	| ID '[' INT_VAR ']'  {
							$$ = new LvalNode($1,$3,NULL,"","",NULL);
						   } 
	| ID '[' INT_VAR ']' '[' INT_VAR ']' {
											$$ = new LvalNode($1,$3,$6,"","",NULL);
										 }
	| ID '['EXPR']' {
						$$ = new LvalNode($1, NULL, NULL,"" ,"" ,$3);
					}
	;


FUNCALLSTM: ID '(' ')' { 
							$$ = new FuncallNode($1); 
						}
		  | ID '(' EXPRLIST ')' { 
		  							$$ = new FuncallNode($1, $3);
		  						}
		  ;

EXPRLIST: EXPRLIST ',' EXPR  { $$->insert($3);} 
		| EXPR  {
					$$ = new ExprlistNode();
					$$->insert($1);
				}
		;

PRINTSTM: PRINT '(' EXPR ')' {$$ = new PrintNode($3);}
		;

READSTM: INPUT  '(' ID ')' {$$ = new ReadNode($3);}
		;


EXPR: ID  {$$ = new ExprNode($1, NULL, NULL, "","",NULL,NULL,symtable);}
	| ID '[' INT_VAR ']' {$$ = new ExprNode($1,$3,NULL,"","",NULL,NULL,symtable);}
	| ID '[' INT_VAR ']' '[' INT_VAR ']'  {$$ = new ExprNode($1,$3,$6,"","",NULL,NULL,symtable);}
	| ID '[' ID ']'  {$$ = new ExprNode($1,NULL,NULL,$3,"",NULL,NULL,symtable);}
	| ID '[' ID ']' '[' ID ']'  {$$ = new ExprNode($1,NULL,NULL,$3,$6,NULL,NULL,symtable);}
	| ID '['EXPR']'  {$$ = new ExprNode($1,NULL,NULL,"","",$3,NULL,symtable);}
	| ID '['EXPR']''['EXPR']' {$$ = new ExprNode($1,NULL,NULL,"","",$3,$6,symtable);}
	| FUNCALLSTM  {$$ = $1;}
	| INT_VAR  {$$ = $1;}
	| CHARLIST  {$$ = $1;}
	| TRUE  {$$ = new BoolNode("TRUE");}
	| FALSE  {$$ = new BoolNode("FALSE");}
	| EXPR OP EXPR  { $$ = new BinopNode($1,$2,$3);}  
	| '(' EXPR ')'  { $$ = new EnclosedNode($2);}
	| %empty
	;

OP: ARITHMETIC {$$ = new OpNode($1);}
	| COMPARISON {$$ = new OpNode($1);}
	| CONDITION {$$ = new OpNode($1);}
	| BOOL_OP{$$ = new OpNode($1);}
	;



%%


int main(int argc,char** argv) 
{
  symtable = new Symboltable();
  FILE *myfile = fopen(argv[1], "r");
  if (!myfile) 
  {
    cout << "I can't open file!" << endl;
    return -1;
  }
  yyin = myfile;

  // Parse through the input:
  yyparse();
  Visitor* dfs;
  dfs=new Visitor();
  start->accept(*dfs);
  printf("\nParsing Over\n");
}

void yyerror(const char *s) {
  cout << "Parse error on line " << lineNum << "!  Message: " << s << endl;
  exit(-1);
}
