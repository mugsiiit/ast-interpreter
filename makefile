scanner: scanner.l parser.y visitor.h ast.h
	bison -v -d -t -x --debug --warnings=none parser.y
	flex scanner.l
	g++ -std=c++14 parser.tab.c lex.yy.c -lfl -o  scanner


