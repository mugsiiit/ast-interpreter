#include <bits/stdc++.h>
using namespace std;


class ProgramStart;
class DeclarationListNode;
class DeclarationNode;
class VarDecStatementNode;
class TypeSpecifierNode;
class FuncDecNode;
class FunctionArgsNode;
class BlockNode;
class StatementListNode;
class StatementNode;
class IfNode;
class WhileNode;
class ForNode;
class JumpNode;
class ReturnNode;
class BreakNode;
class ContNode;
class AssignNode;
class LvalNode;
class FuncallNode;
class ExprlistNode;
class PrintNode;
class ReadNode;
class ExprSuperNode;
class ExprNode;
class BinopNode;
class EnclosedNode;
class OpNode;
class BoolNode;
class IntNode;
class CharNode;
class Sym;



class ASTvisitor
{
public:
	virtual void visit(ProgramStart &node) = 0;
	virtual void visit(DeclarationListNode &node) = 0;
	virtual void visit(DeclarationNode &node) = 0;
	virtual void visit(VarDecStatementNode &node) = 0;
	virtual void visit(TypeSpecifierNode &node) = 0;
	virtual void visit(FuncDecNode &node) = 0;
	virtual void visit(FunctionArgsNode &node) = 0;
	virtual void visit(BlockNode &node) = 0;
	virtual void visit(StatementListNode &node) = 0;
	virtual void visit(StatementNode &node) = 0;
	virtual void visit(IfNode &node) = 0;
	virtual void visit(WhileNode &node) = 0;
	virtual void visit(ForNode &node) = 0;
	virtual void visit(JumpNode &node) = 0;
	virtual void visit(ReturnNode &node) = 0;
	virtual void visit(BreakNode &node) = 0;
	virtual void visit(ContNode &node) = 0;
	virtual void visit(AssignNode &node) = 0;
	virtual void visit(LvalNode &node) = 0;
	virtual void visit(FuncallNode &node) = 0;
	virtual void visit(ExprlistNode &node) = 0;
	virtual void visit(PrintNode &node) = 0;
	virtual void visit(ReadNode &node) = 0;
	virtual void visit(ExprNode &node) = 0;
	virtual void visit(BinopNode &node) = 0;
	virtual void visit(EnclosedNode &node) = 0;
	virtual void visit(OpNode &node) = 0;
	virtual void visit(BoolNode &node) = 0;
	virtual void visit(IntNode &node) = 0;
	virtual void visit(CharNode &node) = 0;
};



// for AST interpreter

class Symboltable
{
public:
	map<string,int> symboltable = {};
	int getter(string var)
	{
		cout<<endl<<"GETTER CALLED"<<endl<<var<<"  "<<symboltable[var]<<endl;
		return symboltable[var];
	}
	void setter(string var, int data)
	{	
		cout<<endl<<"SETTER CALLED"<<endl<<var<<"  "<<data<<endl;
		symboltable.insert(make_pair(var,data));
	}
};



class ASTnode
{
public:
	int data;
	virtual ~ASTnode()
	{
		// do nothing
	}
	virtual void accept(ASTvisitor &V) = 0;
};


class ProgramStart : public ASTnode
{
public:
	ASTnode *dec_list_item;
	virtual void accept(ASTvisitor &v)
	{
	    v.visit(*this);
	}
	ASTnode * getDecListItem()
	{
		return dec_list_item;
	}
};


class DeclarationListNode : public ProgramStart
{
public:
	vector<ASTnode *> dec_list;

	void insert(ASTnode * dec_item)
	{
		dec_list.push_back(dec_item);
	}

	virtual void accept(ASTvisitor &v)
	{
	    v.visit(*this);
	}

	vector<ASTnode *>& getDecList()
	{
		return dec_list;
	}
};



class DeclarationNode : public ASTnode
{
public:
	ASTnode* decl_item;
	DeclarationNode(ASTnode* decl_item) : decl_item(decl_item){}

	virtual void accept(ASTvisitor &v)
	{
	    v.visit(*this);
	}

	ASTnode * getDecItem()
	{
		return decl_item;
	}

};


class VarDecStatementNode : public ASTnode
{
public:
	ASTnode* type;
	string id;
	ASTnode* arg1;
	ASTnode* arg2;
	int  array;

	VarDecStatementNode(ASTnode* type, string id, ASTnode* arg1 = NULL,ASTnode* arg2 = NULL, int array=-1):type(type), id(id), arg1(arg1), arg2(arg2), array(array){}

	virtual void accept(ASTvisitor &v)
	{
	    v.visit(*this);
	}

	ASTnode * getTypeItem()
	{
		return type;
	}
	string getIdItem()
	{
		return id;
	}
	ASTnode* getArg1Item()
	{
		return arg1;
	}
	ASTnode* getArg2Item()
	{
		return arg2;
	}
	int getArrayItem()
	{
		return array;
	}

};


class FuncDecNode : public ASTnode
{
	public:
	ASTnode *type_specifier_item;
	string id;
	ASTnode *func_args_item;
	ASTnode *block_stmnt_item;
	FuncDecNode(ASTnode* type_specifier_item, string id, ASTnode *func_args_item, ASTnode *block_stmnt_item):
			type_specifier_item(type_specifier_item), id(id), func_args_item(func_args_item), block_stmnt_item(block_stmnt_item){}
	
	virtual void accept(ASTvisitor &v)
	{
	    v.visit(*this);
	}

	ASTnode* getTypeSpecifier()
	{
		return type_specifier_item;
	}
	string getID()
	{
		return id;
	}
	ASTnode* getFuncArgs()
	{
		return func_args_item;
	}
	ASTnode* getBlockStmnt()
	{
		return block_stmnt_item;
	}
};


class TypeSpecifierNode : public ASTnode
{
public:
	string type;
	TypeSpecifierNode(string type) : type(type){}
	virtual void accept(ASTvisitor &v)
	{
	    v.visit(*this);
	}
	string getType()
	{
		return type;
	}
};


class StatementNode: public ASTnode
{
public:
};



class FunctionArgsNode : public ASTnode
{
public:
	vector<ASTnode *> arg_list;

	void insert(ASTnode * arg_item)
	{
		arg_list.push_back(arg_item);
	}

	virtual void accept(ASTvisitor &v)
	{
	    v.visit(*this);
	}

	vector<ASTnode *>& getArgList()
	{
		return arg_list;
	}
};


class BlockNode: public ASTnode
{
public:
	ASTnode* dec_list;
	ASTnode* stat_list;
	BlockNode(ASTnode* dec_list, ASTnode* stat_list): dec_list(dec_list), stat_list(stat_list){}
	
	virtual void accept(ASTvisitor &v)
	{
	    v.visit(*this);
	}
	
	ASTnode* getDecList()
	{
		return dec_list;
	}
	ASTnode* getStatList()
	{
		return stat_list;
	}
};


class StatementListNode: public ASTnode
{
public:
	vector<ASTnode*> stat_list;

	void insert(ASTnode * stat_item)
	{
		stat_list.push_back(stat_item);
	}

	virtual void accept(ASTvisitor &v)
	{
	    v.visit(*this);
	}

	vector<ASTnode *>& getStatList()
	{
		return stat_list;
	}
};


class IfNode: public StatementNode{
public:
	ASTnode* expr_item;
	ASTnode* if_block_item;
	ASTnode* else_block_item;

	IfNode(ASTnode* expr_item, ASTnode* if_block_item, ASTnode* else_block_item=NULL):
	expr_item(expr_item), if_block_item(if_block_item), else_block_item(else_block_item){}
	
	virtual void accept(ASTvisitor &v)
	{
	    v.visit(*this);
	}
	
	ASTnode* getExprItem()
	{
		return expr_item;
	}
	ASTnode* getIfItem()
	{
		return if_block_item;
	}
	ASTnode* getElseItem()
	{
		return else_block_item;
	}
};



class LvalNode: public ASTnode
{
public:
	string id;	
	ASTnode* num1;
	ASTnode* num2;
	string id1;
	string id2;
	ASTnode* expr_item;

	LvalNode(string id, ASTnode* num1=NULL,ASTnode* num2=NULL ,string id1="",string id2="", ASTnode* expr_item=NULL):
	id(id), num1(num1), num2(num2), id1(id1), id2(id2), expr_item(expr_item){}

	virtual void accept(ASTvisitor &v)
	{
	    v.visit(*this);
	}

	string getIdItem()
	{
		return id;
	}
	ASTnode* getNum1Item()
	{
		return num1;
	}
	ASTnode* getNum2Item()
	{
		return num2;
	}
	string getId1Item()
	{
		return id1;
	}
	string getId2Item()
	{
		return id2;
	}
	ASTnode * getExprItem()
	{
		return expr_item;
	}
};


class AssignNode: public StatementNode
{
public:
	LvalNode* lval;
	ASTnode* expr_item;

	AssignNode(LvalNode* lval, ASTnode* expr_item): lval(lval), expr_item(expr_item){}
	
	virtual void accept(ASTvisitor &v)
	{
	    v.visit(*this);
	}

	LvalNode* getLvalItem()
	{
		return lval;
	}
	
	ASTnode* getExprItem()
	{
		return expr_item;
	}
};


class ForNode: public StatementNode
{
public:
	AssignNode* assignstm;
	ASTnode* expr;
	ASTnode* assign_end;
	ASTnode* block;

	ForNode(AssignNode* assignstm,ASTnode* expr,ASTnode* assign_end,ASTnode* block):
	assignstm(assignstm), expr(expr), assign_end(assign_end), block(block) {
		cout<<endl<<"Interpreting for loop"<<endl;
		string id=assignstm->getLvalItem()->getIdItem();
		cout<<"FOR LOOP OVER ID: "<<id<<endl;
		// assignstm->accept();
		// int val=1;
		// while(val)
		// {
		// 	expr->accept();
		// 	val=expr->val;
		// 	block->accept();
		// 	assign_end->accept();
		// }
	}


	virtual void accept(ASTvisitor &v)
	{
	    v.visit(*this);
	}

	AssignNode* getAssignItem()
	{
		return assignstm;
	}
	ASTnode* getExprItem()
	{
		return expr;
	}
	ASTnode* getAssignEndItem()
	{
		return assign_end;
	}
	ASTnode* getBlockItem()
	{
		return block;
	}
};


class WhileNode: public StatementNode
{
public:
	ASTnode* expr_item;
	ASTnode* while_block_item;

	WhileNode(ASTnode* expr_item, ASTnode* while_block_item):
	expr_item(expr_item), while_block_item(while_block_item){}

	virtual void accept(ASTvisitor &v)
	{
	    v.visit(*this);
	}

	ASTnode* getExprItem()
	{
		return expr_item;
	}
	ASTnode* getWhileblockItem()
	{
		return while_block_item;
	}
};


class JumpNode: public StatementNode
{
	// Base class for return, continue and break
};

class ReturnNode: public JumpNode
{
public:
	ASTnode* expr_item;

	ReturnNode(ASTnode* expr_item=NULL): expr_item(expr_item){}

	virtual void accept(ASTvisitor &v)
	{
	    v.visit(*this);
	}

	ASTnode* getExprItem()
	{
		return expr_item;
	}
};


class ContNode: public JumpNode
{
public:
	virtual void accept(ASTvisitor &v)
	{
	    v.visit(*this);
	}
};


class BreakNode: public JumpNode
{
public:
	virtual void accept(ASTvisitor &v)
	{
	    v.visit(*this);
	}
};




class PrintNode: public StatementNode
{
public:
	ASTnode* expr_item;

	PrintNode(ASTnode* expr_item): expr_item(expr_item)
	{
		cout<<endl<<"Interpreting pint"<<endl;
		cout<<"PRINT STMT: "<<expr_item->data<<endl;
	}

	virtual void accept(ASTvisitor &v)
	{
	    v.visit(*this);
	}

	ASTnode* getExprItem(){
		return expr_item;
	}
};

class ReadNode: public StatementNode
{
public:
	string id;

	ReadNode(string id): id(id){}
	virtual void accept(ASTvisitor &v)
	{
	    v.visit(*this);
	}

	string getIdItem(){
		return id;
	}
};


class ExprSuperNode: public StatementNode
{
public:
	// Base class for all expressions
};


class ExprlistNode: public ASTnode
{
public:
	vector<ASTnode*> expr_list;

	void insert(ASTnode * expr_item)
	{
		expr_list.push_back(expr_item);
	}

	virtual void accept(ASTvisitor &v)
	{
	    v.visit(*this);
	}

	vector<ASTnode*>& getExprList()
	{
		return expr_list;
	}
};


class FuncallNode: public ExprSuperNode
{
public:
	string id;
	ASTnode* expr_list_item;
	FuncallNode(string id, ASTnode* expr_list_item=NULL) : id(id),expr_list_item(expr_list_item){}
	
	virtual void accept(ASTvisitor &v)
	{
	    v.visit(*this);
	}

	string getIdItem()
	{
		return id;
	}
	ASTnode* getExprListItem()
	{
		return expr_list_item;
	}
};


class ExprNode: public ExprSuperNode{
public:
	string id;	
	ASTnode* num1;
	ASTnode* num2;
	string id1;
	string id2;
	ASTnode* expr_item1;
	ASTnode* expr_item2;

	ExprNode(string id, ASTnode* num1=NULL,ASTnode* num2=NULL ,string id1="",string id2="", ASTnode* expr_item1=NULL,  ASTnode* expr_item2=NULL, Symboltable* sym=NULL):
	id(id), num1(num1), num2(num2), id1(id1), id2(id2), expr_item1(expr_item1), expr_item2(expr_item2)
	{
		if(num1==NULL && num2==NULL)
		{
			if(id1=="" && id2=="")
			{
				if(expr_item1==NULL && expr_item2==NULL)
				{
					this->data=sym->getter(id);
				}
			}
		}
	}

	virtual void accept(ASTvisitor &v)
	{
	    v.visit(*this);
	}

	string getIdItem()
	{
		return id;
	}
	ASTnode* getNum1Item()
	{
		return num1;
	}
	ASTnode* getNum2Item()
	{
		return num2;
	}
	string getId1Item()
	{
		return id1;
	}
	string getId2Item()
	{
		return id2;
	}
	ASTnode * getExprItem1()
	{
		return expr_item1;
	}
	ASTnode * getExprItem2()
	{
		return expr_item2;
	}
};


class OpNode: public ExprSuperNode
{
public:
	string op;

	OpNode(string op): op(op){}

	virtual void accept(ASTvisitor &v)
	{
	    v.visit(*this);
	}

	string getOpItem(){
		return op;
	}
};



class BinopNode: public ExprSuperNode
{
public:
	ASTnode* expr1;
	ASTnode* op;
	ASTnode* expr2;

	BinopNode(ASTnode* expr1, OpNode* op, ASTnode* expr2): expr1(expr1), op(op), expr2(expr2)
	{
		if(op->op=="+")
		{
			this->data=expr1->data + expr2->data;
		}
		else if(op->op=="%")
		{
			this->data = expr1->data % expr2->data;
		}
		else if(op->op=="-")
		{
			this->data = expr1->data - expr2->data;
		}
		else if(op->op=="*")
		{
			this->data = expr1->data * expr2->data;
		}
		else if(op->op=="==")
		{
			this->data = expr1->data == expr2->data;
		}
		else if(op->op=="!=")
		{
			this->data = expr1->data != expr2->data;
		}
		else if(op->op=="<")
		{
			this->data = expr1->data < expr2->data;
		}
		else if(op->op==">")
		{
			this->data = expr1->data > expr2->data;
		}
		else if(op->op=="<=")
		{
			this->data = expr1->data <= expr2->data;
		}
		else if(op->op==">=")
		{
			this->data = expr1->data >= expr2->data;
		}
	}
	virtual void accept(ASTvisitor &v)
	{
	    v.visit(*this);
	}
	
	ASTnode * getExprItem1()
	{
		return expr1;
	}
	ASTnode * getOpItem()
	{
		return op;
	}
	ASTnode * getExprItem2()
	{
		return expr2;
	}
};

class EnclosedNode: public ExprSuperNode
{
public:
	ASTnode* expr_item;

	EnclosedNode(ASTnode* expr_item): expr_item(expr_item)
	{
		this->data=expr_item->data;
	}

	virtual void accept(ASTvisitor &v)
	{
	    v.visit(*this);
	}

	ASTnode* getExprItem()
	{
		return expr_item;
	}
};




class BoolNode: public ExprSuperNode
{
public:
	string boolitem;

	BoolNode(string boolitem): boolitem(boolitem)
	{
		if(boolitem=="FALSE")
			this->data=0;
		else
			this->data=1;
	}

	virtual void accept(ASTvisitor &v)
	{
	    v.visit(*this);
	}

	string getBoolItem()
	{
		return boolitem;
	}
};


class IntNode: public ExprSuperNode
{
public:
	int intitem;

	IntNode(int intitem): intitem(intitem)
	{
		this->data=intitem;
	}

	virtual void accept(ASTvisitor &v)
	{
	    v.visit(*this);
	}

	int getIntItem()
	{
		return intitem;
	}
};


class CharNode: public ExprSuperNode
{
public:
	string charitem;

	CharNode(string charitem): charitem(charitem)
	{
		this->data=int(charitem[0]);
	}

	virtual void accept(ASTvisitor &v)
	{
	    v.visit(*this);
	}

	string getCharItem()
	{
		return charitem;
	}
};