#include "ast.h"



class Visitor: public ASTvisitor 
{
    public:
    	virtual void visit(ProgramStart &node) 
    	{
    		// cout<<endl<<"Program begins"<<endl;
			node.getDecListItem()->accept(*this);
    		// cout<<"Program ends "<<endl;
		}

		virtual void visit(DeclarationListNode &node) 
		{
			// cout<<endl<<"Declaration list begins"<<endl;
			auto x = node.getDecList();
			for(auto y : x)
			{
				y->accept(*this);
			}
			// cout<<"Declaration list ends"<<endl;
		}

		virtual void visit(DeclarationNode &node) 
		{
			// cout<<"Here is a declaration"<<endl;
			node.getDecItem()->accept(*this);	
		}

		virtual void visit(TypeSpecifierNode &node) 
		{
			// cout<<endl<<"This is a type"<<endl;
			cout<<node.getType()<<" ";
		}

		virtual void visit(FuncDecNode &node) 
		{
			// cout<<endl<<"Declaring a function"<<endl;
			node.getTypeSpecifier()->accept(*this);
			cout<<node.getID()<<" ";
			node.getFuncArgs()->accept(*this);
			node.getBlockStmnt()->accept(*this);
			cout<<endl;
		}

		virtual void visit(FunctionArgsNode &node) 
		{
			// cout<<endl<<"Function arguments"<<endl;
			auto x = node.getArgList();
			for(auto y : x)
			{
				y->accept(*this);
			}
			cout<<endl;
		}

		virtual void visit(BlockNode &node) 
		{
			// cout<<endl<<"A block looks like this"<<endl;
			node.getDecList()->accept(*this);
			node.getStatList()->accept(*this);
		}

		virtual void visit(VarDecStatementNode &node) 
		{
			// cout<<endl<<"This is a var declaration"<<endl;
			node.getTypeItem()->accept(*this);
			cout<<node.getIdItem()<<" ";
			auto arg1 = node.getArg1Item();
			auto arg2 = node.getArg2Item();
			int array = node.getArrayItem();
			if(array==1)
			{
				cout<<'['<<" ";
				arg1->accept(*this);
				cout<<']'<<" ";
			}
			else if(array==2)
			{
				cout<<'['<<" ";
				arg1->accept(*this);
				cout<<']'<<" ";
				cout<<'['<<" ";
				arg2->accept(*this);
				cout<<']'<<" ";
			}
			else if(array==3)
			{
				cout<<'['<<" ";
				cout<<']'<<" ";
			}
			else if(array==4)
			{
				cout<<'['<<" ";
				cout<<']'<<" ";
				cout<<'['<<" ";
				cout<<']'<<" ";
			}
			cout<<endl;
		}

		virtual void visit(StatementListNode &node) 
		{
			// cout<<endl<<"This is a statement in the language"<<endl;
			auto x = node.getStatList();
			for(auto y : x)
			{
				y->accept(*this);
			}
		}

		virtual void visit(StatementNode &node) 
		{
			// Useless!
		}

		virtual void visit(IfNode &node) 
		{
			// cout<<endl<<"If statement"<<endl;
			node.getExprItem()->accept(*this);
			node.getIfItem()->accept(*this);

			auto elseblock = node.getElseItem();
			if(elseblock!=NULL)
			{
				node.getElseItem()->accept(*this);
			}
		}

		virtual void visit(WhileNode &node) 
		{
			// cout<<endl<<"While loop"<<endl;
			node.getExprItem()->accept(*this);
			node.getWhileblockItem()->accept(*this);
		}

		virtual void visit(ForNode &node) 
		{
			// cout<<endl<<"For Loop"<<endl;
			node.getAssignItem()->accept(*this);
			node.getExprItem()->accept(*this);
			node.getAssignEndItem()->accept(*this);
			node.getBlockItem()->accept(*this);
		}

		virtual void visit(JumpNode &node) 
		{
			// does nothing
			// inheritance	
		}

		virtual void visit(ReturnNode &node) 
		{
			// cout<<endl<<"Returns control"<<endl;
			auto x = node.getExprItem();
			if(x!=NULL)
			{
				node.getExprItem()->accept(*this);
			}
		}

		virtual void visit(BreakNode &node) 
		{
			// does nothing	
		}

		virtual void visit(ContNode &node) 
		{
			// does nothing	
		}

		virtual void visit(AssignNode &node) 
		{
			// cout<<endl<<"Assignment statement"<<endl;
			node.getLvalItem()->accept(*this);
			cout<<"="<<" ";
			node.getExprItem()->accept(*this);
		}

		virtual void visit(LvalNode &node) 
		{
			// cout<<"Assign value left side "<<endl;
			cout<<node.getIdItem()<<" ";
			auto num1 = node.getNum1Item();
			auto num2 = node.getNum2Item();
			string id1 = node.getId1Item();
			string id2 = node.getId2Item();

			if(num1!=NULL)
			{
				cout<<"["<<" ";
				num1->accept(*this);
				cout<<"]"<<" ";
			}
			if(num2!=NULL)
			{
				cout<<"["<<" ";
				num2->accept(*this);
				cout<<"]"<<" ";
			}
			if(id1!="")
			{
				cout<<"["<<" ";
				cout<<id1<<" ";
				cout<<"]"<<" ";
			}
			if(id2!="")
			{
				cout<<"["<<" ";
				cout<<id2<<" ";
				cout<<"]"<<" ";
			}
			auto x = node.getExprItem();
			if(x!=NULL)
			{
				node.getExprItem()->accept(*this);
			}
			cout<<endl;
		}

		virtual void visit(FuncallNode &node) 
		{
			// cout<<endl<<"Here is a function call"<<endl;
			cout<<node.getIdItem()<<" ";
			auto x = node.getExprListItem();
			if(x!=NULL)
			{
				node.getExprListItem()->accept(*this);
			}
		}

		virtual void visit(ExprlistNode &node) 
		{
			// cout<<endl<<"This is a list of expressions"<<endl;
			auto x = node.getExprList();
			for(auto y : x)
			{
				y->accept(*this);
			}
		}

		virtual void visit(PrintNode &node) 
		{
			node.getExprItem()->accept(*this);
		}

		virtual void visit(ReadNode &node) 
		{
			cout<<node.getIdItem()<<" ";
		}

		virtual void visit(ExprNode &node) 
		{
			// cout<<"This is an expression"<<endl;
			cout<<node.getIdItem()<<" ";
			auto num1 = node.getNum1Item();
			auto num2 = node.getNum2Item();
			string id1 = node.getId1Item();
			string id2 = node.getId2Item();
			if(num1!=NULL)
			{
				cout<<"["<<" ";
				num1->accept(*this);
				cout<<"]"<<" ";
			}
			if(num2!=NULL)
			{
				cout<<"["<<" ";
				num2->accept(*this);
				cout<<"]"<<" ";
			}
			if(id1!="")
			{
				cout<<"["<<" ";
				cout<<id1<<" ";
				cout<<"]"<<" ";
			}
			if(id2!="")
			{
				cout<<"["<<" ";
				cout<<id2<<" ";
				cout<<"]"<<" ";
			}

			auto x = node.getExprItem1();
			if(x!=NULL)
			{
				node.getExprItem1()->accept(*this);
			}

			auto x2 = node.getExprItem2();
			if(x2!=NULL)
			{
				node.getExprItem2()->accept(*this);
			}
		}

		virtual void visit(BinopNode &node) 
		{
			node.getExprItem1()->accept(*this);
			node.getOpItem()->accept(*this);
			node.getExprItem2()->accept(*this);
		}

		virtual void visit(EnclosedNode &node) 
		{
			node.getExprItem()->accept(*this);
		}

		virtual void visit(OpNode &node) 
		{
			cout<<node.getOpItem()<<" ";
		}

		virtual void visit(BoolNode &node) 
		{
			cout<<node.getBoolItem()<<" ";
		}

		virtual void visit(IntNode &node)
		{
			cout<<node.getIntItem()<<" ";
		}

		virtual void visit(CharNode &node)
		{
			cout<<node.getCharItem()<<" ";
		}
};


