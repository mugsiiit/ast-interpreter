%{
	#include <bits/stdc++.h>
    #include "parser.tab.h"
    #include "ast.h"
    using namespace std;
    int lineNum = 1;
%}



%%
\t {}
\n {lineNum++;}
if {return IF;}
print {return PRINT;}
int {return INT;}
input {return INPUT;}
while {return WHILE;}
break {return BREAK;}
for {return FOR;}
return {return RETURN;}
continue {return CONTINUE;}
else {return ELSE;}
bool {return BOOL;}
char {return CHAR;}
void {return VOID;}
true {return TRUE;}
false {return FALSE;}

[;,=\?:\(\)\[\]\{\}] {return yytext[0];}
[a-zA-Z][a-zA-Z0-9]* {yylval.stringval = strdup(yytext); return ID;}
\".*\" {yylval.charnode = new CharNode(strdup(yytext)); return CHARLIST;}
[0-9]+  {yylval.intnode = new IntNode(atoi(yytext)); return INT_VAR;}
&&|\|\| {yylval.stringval = strdup(yytext); return CONDITION;}
\+|\-|\*|\/|% {yylval.stringval = strdup(yytext); return ARITHMETIC;}
==|!=|<=|>=|<|> {yylval.stringval = strdup(yytext); return COMPARISON;}
. {}
%%

